functor GeoriLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Geori_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
structure A = Absyn

open Symbol

fun i(i: int) = A.IntExp(i)

fun i(i: real) = A.FloatExp(i)

fun simpleVar(id: string, pos: int) = A.SimpleVar((symbol id), pos)



end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\002\000\019\000\003\000\018\000\004\000\017\000\005\000\016\000\
\\009\000\015\000\013\000\014\000\014\000\041\000\026\000\013\000\
\\028\000\012\000\031\000\011\000\033\000\010\000\000\000\
\\001\000\002\000\019\000\003\000\018\000\004\000\017\000\005\000\016\000\
\\009\000\015\000\013\000\014\000\026\000\013\000\028\000\012\000\
\\031\000\011\000\033\000\010\000\000\000\
\\001\000\002\000\036\000\032\000\035\000\000\000\
\\001\000\002\000\062\000\029\000\061\000\000\000\
\\001\000\002\000\078\000\000\000\
\\001\000\002\000\089\000\000\000\
\\001\000\002\000\094\000\000\000\
\\001\000\006\000\076\000\010\000\075\000\000\000\
\\001\000\006\000\093\000\010\000\092\000\000\000\
\\001\000\008\000\057\000\000\000\
\\001\000\008\000\090\000\000\000\
\\001\000\008\000\098\000\000\000\
\\001\000\009\000\005\000\000\000\
\\001\000\009\000\022\000\000\000\
\\001\000\009\000\023\000\000\000\
\\001\000\009\000\083\000\000\000\
\\001\000\010\000\006\000\000\000\
\\001\000\010\000\042\000\000\000\
\\001\000\010\000\043\000\000\000\
\\001\000\010\000\063\000\000\000\
\\001\000\010\000\064\000\000\000\
\\001\000\015\000\053\000\016\000\052\000\017\000\051\000\018\000\050\000\
\\019\000\049\000\020\000\048\000\021\000\047\000\022\000\046\000\
\\023\000\045\000\024\000\044\000\000\000\
\\001\000\019\000\049\000\020\000\048\000\021\000\047\000\022\000\046\000\
\\023\000\045\000\024\000\044\000\000\000\
\\001\000\025\000\079\000\000\000\
\\001\000\025\000\095\000\000\000\
\\001\000\029\000\061\000\000\000\
\\001\000\030\000\077\000\000\000\
\\001\000\034\000\004\000\000\000\
\\100\000\000\000\
\\101\000\000\000\
\\102\000\025\000\020\000\000\000\
\\103\000\000\000\
\\104\000\000\000\
\\105\000\000\000\
\\106\000\000\000\
\\107\000\000\000\
\\108\000\000\000\
\\109\000\000\000\
\\110\000\000\000\
\\111\000\027\000\085\000\000\000\
\\112\000\000\000\
\\113\000\000\000\
\\114\000\000\000\
\\115\000\000\000\
\\116\000\000\000\
\\117\000\000\000\
\\118\000\000\000\
\\119\000\002\000\059\000\000\000\
\\120\000\000\000\
\\121\000\002\000\089\000\000\000\
\\122\000\000\000\
\\123\000\000\000\
\\124\000\000\000\
\\125\000\000\000\
\\126\000\000\000\
\\127\000\000\000\
\\128\000\000\000\
\\129\000\000\000\
\\130\000\002\000\019\000\003\000\018\000\004\000\017\000\005\000\016\000\
\\009\000\015\000\013\000\014\000\026\000\013\000\028\000\012\000\
\\031\000\011\000\033\000\010\000\000\000\
\\131\000\000\000\
\\132\000\000\000\
\\133\000\000\000\
\\134\000\000\000\
\\135\000\000\000\
\\136\000\000\000\
\\137\000\000\000\
\\138\000\000\000\
\\139\000\000\000\
\\140\000\000\000\
\\141\000\000\000\
\\142\000\000\000\
\\143\000\000\000\
\\144\000\000\000\
\\145\000\000\000\
\\146\000\009\000\029\000\000\000\
\"
val actionRowNumbers =
"\028\000\029\000\013\000\017\000\
\\002\000\031\000\044\000\030\000\
\\032\000\045\000\014\000\015\000\
\\002\000\002\000\035\000\034\000\
\\033\000\075\000\002\000\003\000\
\\002\000\002\000\001\000\063\000\
\\018\000\019\000\022\000\059\000\
\\010\000\048\000\054\000\047\000\
\\046\000\002\000\004\000\020\000\
\\023\000\021\000\064\000\042\000\
\\037\000\036\000\002\000\002\000\
\\002\000\002\000\002\000\002\000\
\\002\000\002\000\002\000\002\000\
\\060\000\008\000\061\000\038\000\
\\055\000\026\000\027\000\005\000\
\\024\000\002\000\002\000\074\000\
\\073\000\072\000\071\000\070\000\
\\069\000\068\000\067\000\066\000\
\\065\000\058\000\002\000\043\000\
\\016\000\002\000\041\000\040\000\
\\062\000\050\000\011\000\002\000\
\\052\000\051\000\009\000\007\000\
\\057\000\039\000\025\000\006\000\
\\049\000\002\000\053\000\012\000\
\\056\000\000\000"
val gotoT =
"\
\\001\000\097\000\003\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\007\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\019\000\000\000\
\\000\000\
\\000\000\
\\002\000\023\000\009\000\006\000\013\000\005\000\016\000\022\000\000\000\
\\002\000\026\000\009\000\006\000\013\000\005\000\014\000\025\000\
\\015\000\024\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\028\000\009\000\006\000\013\000\005\000\000\000\
\\005\000\032\000\006\000\031\000\007\000\030\000\008\000\029\000\000\000\
\\002\000\036\000\009\000\006\000\013\000\005\000\015\000\035\000\000\000\
\\002\000\036\000\009\000\006\000\013\000\005\000\015\000\037\000\000\000\
\\002\000\038\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\054\000\009\000\006\000\013\000\005\000\017\000\053\000\
\\018\000\052\000\000\000\
\\000\000\
\\007\000\056\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\058\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\063\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\064\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\065\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\066\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\067\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\068\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\069\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\070\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\071\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\072\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\078\000\009\000\006\000\013\000\005\000\000\000\
\\002\000\079\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\080\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\002\000\082\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\010\000\086\000\011\000\085\000\012\000\084\000\000\000\
\\000\000\
\\002\000\089\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\012\000\094\000\000\000\
\\000\000\
\\002\000\095\000\009\000\006\000\013\000\005\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 98
val numrules = 47
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | MAIN of unit ->  (string) | STRING of unit ->  (string)
 | FLOAT of unit ->  (real) | INT of unit ->  (int)
 | ID of unit ->  (string) | explist_nonempty of unit ->  (A.exp list)
 | explist of unit ->  (A.exp list)
 | expseq of unit ->  ( ( A.exp * pos )  list)
 | comp_exp of unit ->  (A.exp) | arith_exp of unit ->  (A.exp)
 | lvalue of unit ->  (A.var) | tyfield of unit ->  (A.field)
 | tyfields_nonempty of unit ->  (A.field list)
 | tyfields of unit ->  (A.field list) | funcall of unit ->  (A.exp)
 | fundeclist of unit ->  (A.fundec list)
 | fundec of unit ->  (A.fundec) | vardec of unit ->  (A.dec)
 | dec of unit ->  (A.dec) | decs of unit ->  (A.dec list)
 | main of unit ->  (A.exp) | exp of unit ->  (A.exp)
 | program of unit ->  (A.exp)
end
type svalue = MlyValue.svalue
type result = A.exp
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 27) => true | (T 28) => true | (T 25) => true | (T 26) => true
 | (T 32) => true | (T 33) => true | (T 30) => true | (T 31) => true
 | (T 29) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
,nil
 $$ (T 8))::
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "ID"
  | (T 2) => "INT"
  | (T 3) => "FLOAT"
  | (T 4) => "STRING"
  | (T 5) => "COMMA"
  | (T 6) => "COLON"
  | (T 7) => "SEMICOLON"
  | (T 8) => "LPAREN"
  | (T 9) => "RPAREN"
  | (T 10) => "LBRACK"
  | (T 11) => "RBRACK"
  | (T 12) => "LBRACE"
  | (T 13) => "RBRACE"
  | (T 14) => "PLUS"
  | (T 15) => "MINUS"
  | (T 16) => "TIMES"
  | (T 17) => "DIVIDE"
  | (T 18) => "EQ"
  | (T 19) => "NEQ"
  | (T 20) => "LT"
  | (T 21) => "LE"
  | (T 22) => "GT"
  | (T 23) => "GE"
  | (T 24) => "ASSIGN"
  | (T 25) => "IF"
  | (T 26) => "ELSE"
  | (T 27) => "WHILE"
  | (T 28) => "FUNCTION"
  | (T 29) => "END"
  | (T 30) => "DEC"
  | (T 31) => "IN"
  | (T 32) => "NIL"
  | (T 33) => "MAIN"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn (T 1) => MlyValue.ID(fn () => ("bogus")) | 
(T 2) => MlyValue.INT(fn () => (0)) | 
(T 3) => MlyValue.FLOAT(fn () => (0.0)) | 
(T 4) => MlyValue.STRING(fn () => ("")) | 
_ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27) $$ (T 26)
 $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19)
 $$ (T 18) $$ (T 17) $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12)
 $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ 
(T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.main main1, main1left, main1right)) :: 
rest671)) => let val  result = MlyValue.program (fn _ => let val  (
main as main1) = main1 ()
 in (main)
end)
 in ( LrTable.NT 0, ( result, main1left, main1right), rest671)
end
|  ( 1, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: _ :: ( _, (
 MlyValue.MAIN MAIN1, (MAINleft as MAIN1left), _)) :: rest671)) => let
 val  result = MlyValue.main (fn _ => let val  (MAIN as MAIN1) = MAIN1
 ()
 val  (exp as exp1) = exp1 ()
 in (
A.Main{name=symbol MAIN,
                                               body=exp,pos=MAINleft}
)
end)
 in ( LrTable.NT 2, ( result, MAIN1left, exp1right), rest671)
end
|  ( 2, ( ( _, ( MlyValue.lvalue lvalue1, lvalue1left, lvalue1right))
 :: rest671)) => let val  result = MlyValue.exp (fn _ => let val  (
lvalue as lvalue1) = lvalue1 ()
 in (A.VarExp(lvalue))
end)
 in ( LrTable.NT 1, ( result, lvalue1left, lvalue1right), rest671)
end
|  ( 3, ( ( _, ( _, NIL1left, NIL1right)) :: rest671)) => let val  
result = MlyValue.exp (fn _ => (A.NilExp))
 in ( LrTable.NT 1, ( result, NIL1left, NIL1right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.INT INT1, INT1left, INT1right)) :: rest671))
 => let val  result = MlyValue.exp (fn _ => let val  (INT as INT1) = 
INT1 ()
 in (A.IntExp INT)
end)
 in ( LrTable.NT 1, ( result, INT1left, INT1right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.FLOAT FLOAT1, FLOAT1left, FLOAT1right)) :: 
rest671)) => let val  result = MlyValue.exp (fn _ => let val  (FLOAT
 as FLOAT1) = FLOAT1 ()
 in (A.FloatExp FLOAT)
end)
 in ( LrTable.NT 1, ( result, FLOAT1left, FLOAT1right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.STRING STRING1, (STRINGleft as STRING1left),
 STRING1right)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (STRING as STRING1) = STRING1 ()
 in (A.StringExp (STRING, STRINGleft))
end)
 in ( LrTable.NT 1, ( result, STRING1left, STRING1right), rest671)
end
|  ( 7, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.arith_exp 
arith_exp1, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let
 val  result = MlyValue.exp (fn _ => let val  (arith_exp as arith_exp1
) = arith_exp1 ()
 in (arith_exp)
end)
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 8, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.comp_exp 
comp_exp1, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let
 val  result = MlyValue.exp (fn _ => let val  (comp_exp as comp_exp1)
 = comp_exp1 ()
 in (comp_exp)
end)
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 9, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.exp exp1,
 _, _)) :: _ :: ( _, ( MlyValue.lvalue lvalue1, (lvalueleft as 
lvalue1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (lvalue as lvalue1) = lvalue1 ()
 val  (exp as exp1) = exp1 ()
 in (A.AssignExp {var=lvalue, exp=exp, pos=lvalueleft})
end)
 in ( LrTable.NT 1, ( result, lvalue1left, SEMICOLON1right), rest671)

end
|  ( 10, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.comp_exp comp_exp1,
 _, _)) :: _ :: ( _, ( _, (IFleft as IF1left), _)) :: rest671)) => let
 val  result = MlyValue.exp (fn _ => let val  (comp_exp as comp_exp1)
 = comp_exp1 ()
 val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.IfExp {test=comp_exp, then'=exp1, else'=SOME(exp2), pos=IFleft}
)
end)
 in ( LrTable.NT 1, ( result, IF1left, exp2right), rest671)
end
|  ( 11, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.comp_exp comp_exp1, _, _)) :: _ :: ( _, ( _, (IFleft as 
IF1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _ =>
 let val  (comp_exp as comp_exp1) = comp_exp1 ()
 val  exp1 = exp1 ()
 in (A.IfExp {test=comp_exp, then'=exp1, else'=NONE, pos=IFleft})
end)
 in ( LrTable.NT 1, ( result, IF1left, exp1right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.comp_exp comp_exp1, _, _)) :: _ :: ( _, ( _, (WHILEleft as 
WHILE1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (comp_exp as comp_exp1) = comp_exp1 ()
 val  exp1 = exp1 ()
 in (A.WhileExp {test=comp_exp, body=exp1, pos=WHILEleft})
end)
 in ( LrTable.NT 1, ( result, WHILE1left, exp1right), rest671)
end
|  ( 13, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.expseq 
expseq1, _, _)) :: ( _, ( _, LBRACE1left, _)) :: rest671)) => let val 
 result = MlyValue.exp (fn _ => let val  (expseq as expseq1) = expseq1
 ()
 in (A.SeqExp expseq)
end)
 in ( LrTable.NT 1, ( result, LBRACE1left, RBRACE1right), rest671)
end
|  ( 14, ( ( _, ( _, _, END1right)) :: ( _, ( MlyValue.exp exp1, _, _)
) :: _ :: ( _, ( MlyValue.decs decs1, _, _)) :: ( _, ( _, (DECleft as 
DEC1left), _)) :: rest671)) => let val  result = MlyValue.exp (fn _ =>
 let val  (decs as decs1) = decs1 ()
 val  (exp as exp1) = exp1 ()
 in (A.DecExp {decs=decs, body=exp, pos=DECleft})
end)
 in ( LrTable.NT 1, ( result, DEC1left, END1right), rest671)
end
|  ( 15, ( ( _, ( MlyValue.funcall funcall1, funcall1left, 
funcall1right)) :: rest671)) => let val  result = MlyValue.exp (fn _
 => let val  (funcall as funcall1) = funcall1 ()
 in (funcall)
end)
 in ( LrTable.NT 1, ( result, funcall1left, funcall1right), rest671)

end
|  ( 16, ( rest671)) => let val  result = MlyValue.decs (fn _ => ([]))
 in ( LrTable.NT 3, ( result, defaultPos, defaultPos), rest671)
end
|  ( 17, ( ( _, ( MlyValue.dec dec1, _, dec1right)) :: ( _, ( 
MlyValue.decs decs1, decs1left, _)) :: rest671)) => let val  result = 
MlyValue.decs (fn _ => let val  (decs as decs1) = decs1 ()
 val  (dec as dec1) = dec1 ()
 in (decs @ [dec])
end)
 in ( LrTable.NT 3, ( result, decs1left, dec1right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.vardec vardec1, vardec1left, vardec1right))
 :: rest671)) => let val  result = MlyValue.dec (fn _ => let val  (
vardec as vardec1) = vardec1 ()
 in (vardec)
end)
 in ( LrTable.NT 4, ( result, vardec1left, vardec1right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.fundeclist fundeclist1, fundeclist1left, 
fundeclist1right)) :: rest671)) => let val  result = MlyValue.dec (fn
 _ => let val  (fundeclist as fundeclist1) = fundeclist1 ()
 in (A.FunctionDec fundeclist)
end)
 in ( LrTable.NT 4, ( result, fundeclist1left, fundeclist1right), 
rest671)
end
|  ( 20, ( ( _, ( MlyValue.ID ID2, _, ID2right)) :: ( _, ( MlyValue.ID
 ID1, ID1left, _)) :: rest671)) => let val  result = MlyValue.tyfield
 (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 in ({name=symbol ID2, escape=ref true, typ=symbol ID1, pos=ID1left})

end)
 in ( LrTable.NT 11, ( result, ID1left, ID2right), rest671)
end
|  ( 21, ( rest671)) => let val  result = MlyValue.tyfields (fn _ => (
[]))
 in ( LrTable.NT 9, ( result, defaultPos, defaultPos), rest671)
end
|  ( 22, ( ( _, ( MlyValue.tyfields_nonempty tyfields_nonempty1, 
tyfields_nonempty1left, tyfields_nonempty1right)) :: rest671)) => let
 val  result = MlyValue.tyfields (fn _ => let val  (tyfields_nonempty
 as tyfields_nonempty1) = tyfields_nonempty1 ()
 in (tyfields_nonempty)
end)
 in ( LrTable.NT 9, ( result, tyfields_nonempty1left, 
tyfields_nonempty1right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.tyfield tyfield1, tyfield1left, 
tyfield1right)) :: rest671)) => let val  result = 
MlyValue.tyfields_nonempty (fn _ => let val  (tyfield as tyfield1) = 
tyfield1 ()
 in ([tyfield])
end)
 in ( LrTable.NT 10, ( result, tyfield1left, tyfield1right), rest671)

end
|  ( 24, ( ( _, ( MlyValue.tyfield tyfield1, _, tyfield1right)) :: _
 :: ( _, ( MlyValue.tyfields tyfields1, tyfields1left, _)) :: rest671)
) => let val  result = MlyValue.tyfields_nonempty (fn _ => let val  (
tyfields as tyfields1) = tyfields1 ()
 val  (tyfield as tyfield1) = tyfield1 ()
 in (tyfields @ [tyfield])
end)
 in ( LrTable.NT 10, ( result, tyfields1left, tyfield1right), rest671)

end
|  ( 25, ( ( _, ( MlyValue.fundec fundec1, fundec1left, fundec1right))
 :: rest671)) => let val  result = MlyValue.fundeclist (fn _ => let
 val  (fundec as fundec1) = fundec1 ()
 in ([fundec])
end)
 in ( LrTable.NT 7, ( result, fundec1left, fundec1right), rest671)
end
|  ( 26, ( ( _, ( MlyValue.fundec fundec1, _, fundec1right)) :: ( _, (
 MlyValue.fundeclist fundeclist1, fundeclist1left, _)) :: rest671)) =>
 let val  result = MlyValue.fundeclist (fn _ => let val  (fundeclist
 as fundeclist1) = fundeclist1 ()
 val  (fundec as fundec1) = fundec1 ()
 in (fundeclist @ [fundec])
end)
 in ( LrTable.NT 7, ( result, fundeclist1left, fundec1right), rest671)

end
|  ( 27, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.exp exp1,
 _, _)) :: _ :: _ :: ( _, ( MlyValue.tyfields tyfields1, _, _)) :: _
 :: ( _, ( MlyValue.ID ID2, _, _)) :: ( _, ( _, FUNCTIONleft, _)) :: (
 _, ( MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.fundec (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 val  (tyfields as tyfields1) = tyfields1 ()
 val  (exp as exp1) = exp1 ()
 in (
{result=SOME (symbol ID1, ID1left),
                                                         name=symbol ID2, params=tyfields,
                                                         body=exp,
                                                         pos=FUNCTIONleft}
)
end)
 in ( LrTable.NT 6, ( result, ID1left, SEMICOLON1right), rest671)
end
|  ( 28, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.exp exp1,
 _, _)) :: _ :: ( _, ( MlyValue.ID ID2, ID2left, _)) :: ( _, ( 
MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.vardec (fn _ => let val  ID1 = ID1 ()
 val  ID2 = ID2 ()
 val  (exp as exp1) = exp1 ()
 in (
A.VarDec {typ=SOME (symbol ID1, ID1left),
                                              name=symbol ID2, escape= ref true,
                                                  init=exp, pos=ID2left}
)
end)
 in ( LrTable.NT 5, ( result, ID1left, SEMICOLON1right), rest671)
end
|  ( 29, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.explist 
explist1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, (IDleft as ID1left),
 _)) :: rest671)) => let val  result = MlyValue.funcall (fn _ => let
 val  (ID as ID1) = ID1 ()
 val  (explist as explist1) = explist1 ()
 in (A.CallExp {func=symbol ID, args=explist, pos=IDleft})
end)
 in ( LrTable.NT 8, ( result, ID1left, RPAREN1right), rest671)
end
|  ( 30, ( rest671)) => let val  result = MlyValue.explist (fn _ => (
[]))
 in ( LrTable.NT 16, ( result, defaultPos, defaultPos), rest671)
end
|  ( 31, ( ( _, ( MlyValue.explist_nonempty explist_nonempty1, 
explist_nonempty1left, explist_nonempty1right)) :: rest671)) => let
 val  result = MlyValue.explist (fn _ => let val  (explist_nonempty
 as explist_nonempty1) = explist_nonempty1 ()
 in (explist_nonempty)
end)
 in ( LrTable.NT 16, ( result, explist_nonempty1left, 
explist_nonempty1right), rest671)
end
|  ( 32, ( ( _, ( MlyValue.exp exp1, exp1left, exp1right)) :: rest671)
) => let val  result = MlyValue.explist_nonempty (fn _ => let val  (
exp as exp1) = exp1 ()
 in ([exp])
end)
 in ( LrTable.NT 17, ( result, exp1left, exp1right), rest671)
end
|  ( 33, ( ( _, ( MlyValue.exp exp1, _, exp1right)) :: _ :: ( _, ( 
MlyValue.explist explist1, explist1left, _)) :: rest671)) => let val  
result = MlyValue.explist_nonempty (fn _ => let val  (explist as 
explist1) = explist1 ()
 val  (exp as exp1) = exp1 ()
 in (explist @ [exp])
end)
 in ( LrTable.NT 17, ( result, explist1left, exp1right), rest671)
end
|  ( 34, ( ( _, ( MlyValue.exp exp1, (expleft as exp1left), exp1right)
) :: rest671)) => let val  result = MlyValue.expseq (fn _ => let val 
 (exp as exp1) = exp1 ()
 in ([(exp, expleft)])
end)
 in ( LrTable.NT 15, ( result, exp1left, exp1right), rest671)
end
|  ( 35, ( ( _, ( MlyValue.exp exp1, expleft, exp1right)) :: ( _, ( 
MlyValue.expseq expseq1, expseq1left, _)) :: rest671)) => let val  
result = MlyValue.expseq (fn _ => let val  (expseq as expseq1) = 
expseq1 ()
 val  (exp as exp1) = exp1 ()
 in (expseq @ [(exp, expleft)])
end)
 in ( LrTable.NT 15, ( result, expseq1left, exp1right), rest671)
end
|  ( 36, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.arith_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.PlusOp, right=exp2, pos=exp1left})
end
)
 in ( LrTable.NT 13, ( result, exp1left, exp2right), rest671)
end
|  ( 37, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.arith_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.MinusOp, right=exp2, pos=exp1left})

end)
 in ( LrTable.NT 13, ( result, exp1left, exp2right), rest671)
end
|  ( 38, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.arith_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.TimesOp, right=exp2, pos=exp1left})

end)
 in ( LrTable.NT 13, ( result, exp1left, exp2right), rest671)
end
|  ( 39, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.arith_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.DivideOp, right=exp2, pos=exp1left})

end)
 in ( LrTable.NT 13, ( result, exp1left, exp2right), rest671)
end
|  ( 40, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.comp_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.EqOp, right=exp2, pos=exp1left})
end)
 in ( LrTable.NT 14, ( result, exp1left, exp2right), rest671)
end
|  ( 41, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.comp_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.NeqOp,right=exp2, pos=exp1left})
end)
 in ( LrTable.NT 14, ( result, exp1left, exp2right), rest671)
end
|  ( 42, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.comp_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.LtOp, right=exp2, pos=exp1left})
end)
 in ( LrTable.NT 14, ( result, exp1left, exp2right), rest671)
end
|  ( 43, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.comp_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.LeOp, right=exp2, pos=exp1left})
end)
 in ( LrTable.NT 14, ( result, exp1left, exp2right), rest671)
end
|  ( 44, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.comp_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.GtOp, right=exp2, pos=exp1left})
end)
 in ( LrTable.NT 14, ( result, exp1left, exp2right), rest671)
end
|  ( 45, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( 
MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = 
MlyValue.comp_exp (fn _ => let val  exp1 = exp1 ()
 val  exp2 = exp2 ()
 in (A.OpExp {left=exp1, oper=A.GeOp, right=exp2, pos=exp1left})
end)
 in ( LrTable.NT 14, ( result, exp1left, exp2right), rest671)
end
|  ( 46, ( ( _, ( MlyValue.ID ID1, (IDleft as ID1left), ID1right)) :: 
rest671)) => let val  result = MlyValue.lvalue (fn _ => let val  (ID
 as ID1) = ID1 ()
 in (simpleVar(ID, IDleft))
end)
 in ( LrTable.NT 12, ( result, ID1left, ID1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Geori_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.ID (fn () => i),p1,p2))
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.INT (fn () => i),p1,p2))
fun FLOAT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.FLOAT (fn () => i),p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.STRING (fn () => i),p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun LE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun GE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun FUNCTION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun DEC (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun IN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun NIL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun MAIN (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.MAIN (fn () => i),p1,p2))
end
end
