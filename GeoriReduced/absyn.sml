structure Absyn =
struct

type pos = int   
type symbol = Symbol.symbol


datatype var = SimpleVar of symbol * pos


and exp = VarExp of var
        | NilExp
        | IntExp of int
        | FloatExp of real
        | StringExp of string * pos
        | CallExp of {func: symbol, args: exp list, pos: pos}
        | OpExp of {left: exp, oper: oper, right: exp, pos: pos}
        | SeqExp of (exp * pos) list
        | AssignExp of {var: var, exp: exp, pos: pos}
        | IfExp of {test: exp, then': exp, else': exp option, pos: pos}
        | WhileExp of {test: exp, body: exp, pos: pos}
        | DecExp of {decs: dec list, body: exp, pos: pos}
        | Main of {name: symbol,body: exp,pos: pos}  
        | Print of {name: symbol,body: exp,pos: pos} 
        | ArrayExp of {typ: symbol, size: exp, init: exp, pos: pos}
        

and dec = FunctionDec of fundec list
        | VarDec of {name: symbol,
		     escape: bool ref,
		     typ: (symbol * pos) option,
		     init: exp,
		     pos: pos}
       

and ty
  = NameTy of symbol * pos

        

and oper = PlusOp | MinusOp | TimesOp | DivideOp
         | EqOp | NeqOp | LtOp | LeOp | GtOp | GeOp

withtype field = {name: symbol, escape: bool ref, 
		  typ: symbol, pos: pos}
   and   fundec = {name: symbol,
		   params: field list,
                   result: (symbol * pos) option,
		   body: exp,
		   pos: pos}
     
end
