#!/bin/bash

# Explicit call to ml-yacc
ml-yacc geori.grm &&
  ml-build sources.cm Main.main &&
  head -1 geori.grm.desc &&
  (
    
    files=$(ls ../testCases/*.ri)
    for georiSource in $files; do
      echo -- $georiSource --
      cat $georiSource
      echo --
      ./georic $georiSource
      echo
    done
  ) > run.actual.out &&
 (
  diff -U7 run.expected.out run.actual.out && echo "SUCCESS!"
  ) | tee run.diff.out
echo status=$?
