type pos = int
type svalue = Tokens.svalue
type ('a, 'b) token = ('a, 'b) Tokens.token
type lexresult = (svalue, pos) token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
fun err(p1,p2) = ErrorMsg.error p1

fun eof() = let val pos = hd(!linePos) in Tokens.EOF(pos,pos) end

fun areaSquare(lado: int):int= (lado*lado)

fun areaRectangle(base: int, altura: int) :int= (base*altura)

fun areaCircle(raio: int):real = (Math.pi*Real.fromInt(raio)*Real.fromInt(raio))

fun areaDiamond(diametroMaior: int, diametroMenor: int):int = (diametroMaior*diametroMenor) div 2

fun areaTriangle(base: int, altura: int):int = (base*altura) div 2

fun sub(baseMaior:int, baseMenor:int):real = Real.fromInt(baseMaior)-Real.fromInt(baseMenor)

fun potencia(lado: int, baseMaior: int, baseMenor:int):real = (Real.fromInt(lado)*Real.fromInt(lado))-(sub(baseMaior,baseMenor)*sub(baseMaior,baseMenor))

fun sqrtRoot(pot:real):real = Math.sqrt(pot)
 
fun altura(lado: int, baseMaior: int, baseMenor:int):real = sqrtRoot(potencia(lado, baseMaior, baseMenor))

fun calculo(lado: int, baseMaior: int, baseMenor:int):real = ((Real.fromInt(baseMaior)+Real.fromInt(baseMenor)) * altura(lado, baseMaior, baseMenor))

fun areaTrapeze(lado: int, baseMaior: int, baseMenor:int):real = (calculo(lado, baseMaior, baseMenor) / 2.0)

fun perimeterSquare(lado:int):int = (4*lado)

fun perimeterRectangle(lado1:int, lado2:int):int = (2*lado1)+(2*lado2)

fun perimeterTriangle(lado1:int, lado2:int, lado3:int):int = (lado1+lado2+lado3)

fun perimeterTrapeze(lado:int, baseMaior:int, baseMenor:int):int = (baseMaior+(2*lado)+baseMenor)

fun perimeterDiamond(lado:int):int = (4*lado)

fun circunference(raio: int):real = (2.0*Math.pi*Real.fromInt(raio))



fun stringWithoutQuotes(s: string) =
  String.extract (s, 1, SOME(String.size s - 2))


exception NotAnInt

fun getInt(optionInt : int option) = case optionInt of
  SOME(n) => n
  | _ => raise NotAnInt 


exception NotAnFloat

fun getFloat(optionFloat : real option) = case optionFloat of
  SOME(n) => n
  | _ => raise NotAnFloat 

%%
%header (functor GeoriLexFun(structure Tokens: Geori_TOKENS));
%s COMMENT;
lowerCase = [a-z];
upperCase = [A-Z];
letter = {lowerCase}|{upperCase};
digit = [0-9];
digit_without_zero = [1-9];
integer={digit}+;
float={digit}+"."{digit}+;
id = {lowerCase}({letter}|{digit})*;
quote = ["];
notQuote = [^"];
space = [\ \t];

%%

<INITIAL>"function" => (Tokens.FUNCTION(yypos, yypos + size yytext));
<INITIAL>"nil"      => (Tokens.NIL(yypos, yypos + size yytext));
<INITIAL>"while"    => (Tokens.WHILE(yypos, yypos + size yytext));
<INITIAL>"else"     => (Tokens.ELSE(yypos, yypos + size yytext));
<INITIAL>"if"       => (Tokens.IF(yypos, yypos + size yytext));
<INITIAL>"dec"       => (Tokens.DEC(yypos, yypos + size yytext));
<INITIAL>"end"       => (Tokens.END(yypos, yypos + size yytext));
<INITIAL>"in"       => (Tokens.IN(yypos, yypos + size yytext));
<INITIAL>"main"     => (Tokens.MAIN(yytext, yypos, yypos + size yytext));



<INITIAL>"=" => (Tokens.ASSIGN(yypos, yypos + size yytext));
<INITIAL>">=" => (Tokens.GE(yypos, yypos + size yytext));
<INITIAL>">"  => (Tokens.GT(yypos, yypos + size yytext));
<INITIAL>"<=" => (Tokens.LE(yypos, yypos + size yytext));
<INITIAL>"<"  => (Tokens.LT(yypos, yypos + size yytext));
<INITIAL>"!=" => (Tokens.NEQ(yypos, yypos + size yytext));
<INITIAL>"=="  => (Tokens.EQ(yypos, yypos + size yytext));
<INITIAL>"/"  => (Tokens.DIVIDE(yypos, yypos + size yytext));
<INITIAL>"*"  => (Tokens.TIMES(yypos, yypos + size yytext));
<INITIAL>"-"  => (Tokens.MINUS(yypos, yypos + size yytext));
<INITIAL>"+"  => (Tokens.PLUS(yypos, yypos + size yytext));

<INITIAL>"{" => (Tokens.LBRACE(yypos, yypos + size yytext));
<INITIAL>"}" => (Tokens.RBRACE(yypos, yypos + size yytext));

<INITIAL>"(" => (Tokens.LPAREN(yypos, yypos + size yytext));
<INITIAL>")" => (Tokens.RPAREN(yypos, yypos + size yytext));

<INITIAL>"[" => (Tokens.LBRACK(yypos, yypos + size yytext));
<INITIAL>"]" => (Tokens.RBRACK(yypos, yypos + size yytext));


<INITIAL>"," => (Tokens.COMMA(yypos, yypos + size yytext));
<INITIAL>";" => (Tokens.SEMICOLON(yypos, yypos + size yytext));


<INITIAL>{id}                       => (Tokens.ID(yytext, yypos, yypos + size yytext));
<INITIAL>{integer}                  => (Tokens.INT(getInt (Int.fromString yytext), yypos, yypos + size yytext));
<INITIAL>{float}                    => (Tokens.FLOAT(getFloat (Real.fromString yytext), yypos, yypos + size yytext));
<INITIAL>{quote}{notQuote}*{quote}  => (Tokens.STRING(stringWithoutQuotes(yytext), yypos, yypos + size yytext));





<COMMENT>.      => (continue());

{space}+        => (continue());
"\n"            => (lineNum := !lineNum+1; linePos := yypos :: !linePos; continue());
.               => (ErrorMsg.error yypos ("illegal character '" ^ yytext ^ "'"); continue());
