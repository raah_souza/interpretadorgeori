structure Env: sig
  type access

  type venv
  type tenv

  datatype enventry
    = VarEntry of {access: TL.access, ty: Types.ty}
    | FunEntry of {
        level: TL.level,
        label: Temp.label,
        formals: Types.ty list,
        result: Types.ty
      }

  val base_tenv: tenv (* predefined types *)
  val base_venv: venv (* predefined functions *)
end =
struct
  structure S = Symbol
  structure T = Types

  type access = unit 

  datatype enventry
    = VarEntry of {access: TL.access, ty: Types.ty}
    | FunEntry of {
        level: TL.level,
        label: Temp.label,
        formals: Types.ty list,
        result: Types.ty
      }

  type tenv = T.ty S.table
  type venv = enventry S.table

  val predefinedTypes =
    [("int", T.INT)
    ,("string", T.STRING)
    ,("float", T.FLOAT)
    ,("unit", T.UNIT) 
    ]

  fun enterTy((name, ty), tenv) = S.enter(tenv, S.symbol name, ty)
  val base_tenv = List.foldr enterTy S.empty predefinedTypes

  fun globalFun name formals result =
    (name, FunEntry {level=TL.outermostLevel,
                     label=Temp.namedLabel name,
                     formals=formals,
                     result=result})
 
  val predefinedVars =
    [("nil", VarEntry {access=TL.globalAccess, ty=T.NIL})
    ,globalFun "prints"  [T.STRING]  T.UNIT
    ,globalFun "printi"  [T.INT]  T.UNIT
    ,globalFun "printf"  [T.FLOAT]  T.UNIT
    ,globalFun "areaSquare"  [T.INT]  T.INT
    ,globalFun "areaRectangle"  [T.INT, T.INT]  T.INT
    ,globalFun "areaTrapeze"  [T.INT, T.INT, T.INT]  T.FLOAT
    ,globalFun "areaDiamond"  [T.INT, T.INT]  T.INT
    ,globalFun "areaTriangle"  [T.INT, T.INT]  T.INT
    ,globalFun "areaCircle"  [T.INT]  T.FLOAT
    ,globalFun "perimeterSquare"  [T.INT]  T.INT
    ,globalFun "perimeterRectangle"  [T.INT, T.INT]  T.INT
    ,globalFun "perimeterDiamond"  [T.INT]  T.INT
    ,globalFun "perimeterTriangle"  [T.INT, T.INT, T.INT]  T.INT
    ,globalFun "perimeterTrapeze"  [T.INT, T.INT, T.INT]  T.INT
    ,globalFun "circunference"  [T.INT]  T.FLOAT
    ,globalFun "substring" [T.STRING, T.INT, T.INT] T.STRING
    ,globalFun "concat" [T.STRING, T.STRING] T.STRING
    ]

  fun enterVar((name, enventry), venv) = S.enter (venv, S.symbol name, enventry)
  val base_venv = List.foldr enterVar S.empty predefinedVars
end
