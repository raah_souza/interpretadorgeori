structure Semant:
sig
  val transProg: Absyn.exp -> unit
end =
struct
  structure A = Absyn
  structure P = PrintAbsyn
  structure S = Symbol
  structure E = Env
  structure T = Types


  val doAccess = TL.globalAccess 
  val doTy = T.INT 
  val doTrExp = TL.todoExp 
  val doExpTy = {exp=doTrExp, ty=doTy} 

  (* Value to use when expression is in error and no better value/ty can be provided *)
  val errorTrExpTy = {exp=doTrExp, ty=T.NIL}
  val errorTrExp = TL.todoExp 

  val wouldBeBooleanTy = T.INT

  val error = ErrorMsg.error

  fun lookupActualType(pos, tenv, ty) =
    case S.look(tenv, ty)
      of SOME ty => actual_ty(pos, tenv, ty)
       | NONE   => (error pos ("Type '" ^ S.name ty ^ "' is not defined"); T.NIL)

  and  actual_ty(pos, tenv, ty) = ty

  fun checkInt({exp, ty}, pos) =
    case ty
      of T.INT => ()
      | _ => error pos "Type 'int' required"
  
   fun checkFloat({exp, ty}, pos) =
    case ty
      of T.FLOAT => ()
      | _ => error pos "Type 'float' required"

  fun checkUnit({exp, ty}, pos) =
    case ty
      of T.UNIT => ()
      | _ => error pos "unit required"


  fun reqSameType(pos, tenv, {exp=_, ty=ty1}, {exp=_, ty=ty2}) =
    let val t1 = actual_ty(pos, tenv, ty1)
        val t2 = actual_ty(pos, tenv, ty2)
    in
      if t1 <> t2 then error pos "types do not match"
      else ()
    end
 
  fun findVarType(level, tenv, venv, A.SimpleVar (sym, pos)) =
    (case S.look(venv, sym)
      of SOME(E.VarEntry {access=_, ty}) => actual_ty (pos, tenv, ty)
       | SOME(E.FunEntry _) => (error pos "Cannot assign to a function"; T.NIL)
       | _ => (error pos "Variable does not exist"; T.NIL)
    )


  and transVar(venv, tenv, var) = doExpTy

 and transDec(level, venv, tenv, dec) =
    let
      fun trDec(venv, tenv, A.VarDec {name, escape, typ=NONE, init, pos}) =
        let 
          val {exp=_ , ty} = transExp(level, venv, tenv, init)
          val access = TL.allocLocal level (!escape)
        in 
          {tenv=tenv, venv=S.enter(venv, name, E.VarEntry {access=access, ty=ty})} (* XXX *)
        end

      |   trDec(venv, tenv, A.VarDec {name, escape, typ=SOME(symbol, decTyPos), init, pos}) =
        let val {exp=_ , ty} = transExp(level, venv, tenv, init)
            val decTy = lookupActualType(pos, tenv, symbol)
            val access = TL.allocLocal level (!escape)
        in
          reqSameType(pos, tenv, {exp=(), ty=decTy}, {exp=(), ty=ty});
          (* continue with declared type *)
          {tenv=tenv, venv=S.enter(venv, name, E.VarEntry {access=access, ty=decTy})}
        end

  
  |  trDec(venv, tenv, A.FunctionDec funDecs) =
        let
          fun computeResultType (tenv, result) =
            (case result
               of SOME (resTySym, resPos) => lookupActualType (resPos, tenv, resTySym)
                | NONE => T.UNIT)

          fun transFunDecs(venv, tenv, funDecs) =
            let
              fun transFunDec({name, params, body, pos, result}) =
                let
                  val resTy = computeResultType (tenv, result)
                  fun transParam {name, escape, typ, pos} = {name=name, ty=lookupActualType(pos, tenv, typ)}
                  val params' = map transParam params (* map [ty] => [{name, ty}] *)
                  fun enterParam({name, ty}, venv) = S.enter(venv, name, E.VarEntry {access=doAccess, ty=ty})
                  val venv' = foldl enterParam venv params'
                  val bodyA = transExp(level, venv', tenv, body);
                in
                  reqSameType(pos, tenv, bodyA, {exp=(), ty=resTy})
                end
            in
              app transFunDec funDecs
            end

          fun enterFunHeader({name, params, body, pos, result}, venv) =
            let
              val resTy = computeResultType (tenv, result)
              fun transParam {name, escape, typ, pos} = {name=name, ty=lookupActualType(pos, tenv, typ)}
              val params' = map transParam params (* map [ty] => [{name, ty}] *)
              val label = Temp.newLabel ()
              fun selectEscape {name, escape, typ, pos} = !escape
              val newLevel = TL.newLevel {parent = level, name = label, formals = map selectEscape params}
            in
              S.enter(venv, name, 
                      E.FunEntry {
                        level=newLevel, 
                        label=label,
                        formals=map #ty params', 
                        result=resTy})
            end
          val venv' = foldl enterFunHeader venv funDecs
        in
          transFunDecs (venv', tenv, funDecs);
          {venv=venv', tenv=tenv}
        end

    in 
      trDec(venv, tenv, dec)
    end

  and transDecs(level, venv, tenv, []) = {venv=venv, tenv=tenv}
    | transDecs(level, venv, tenv, dec::decs) =
       let 
         val {tenv=tenv', venv=venv'} = transDec(level, venv, tenv, dec)
       in 
         transDecs(level, venv', tenv', decs)
       end

  and transExp(level, venv, tenv, exp) =
    let
      fun trexp(A.NilExp) = {exp=TL.nil (), ty=T.UNIT}

        | trexp(A.VarExp var) = {exp=doTrExp, ty=findVarType(level, tenv, venv, var)}

        | trexp(A.AssignExp {var, exp, pos}) =
          let
            val expA = trexp exp
            val varTy = findVarType(level, tenv, venv, var)
          in
            reqSameType(pos, tenv, {exp=(), ty=varTy}, expA);
            {exp=doTrExp, ty=T.UNIT}
          end
        
        | trexp(A.WhileExp {test=testExp, body=bodyExp, pos}) =
          let
            val testA = trexp testExp
            val bodyA = trexp bodyExp
          in
            checkInt(testA, pos);
            checkUnit(bodyA, pos);
            {exp=doTrExp, ty=T.UNIT}
          end

        | trexp(A.CallExp {func, args, pos}) =
          let
          in
            case S.look(venv, func)
              of SOME(E.VarEntry _) => (error pos "Variable is not a function"; errorTrExpTy)
               | NONE => (error pos "Function does not exist"; errorTrExpTy)
               | SOME(E.FunEntry {level=_, label=_, formals, result=resTy}) =>
                  let
                    val formalsN = length formals
                    val actualsN = length args
                  in
                    if formalsN <> actualsN then
                      (error pos "Function has the wrong arity"; errorTrExpTy)
                    else
                      let
                        val z = ListPair.zip (args, formals)
                        fun checkType(argExp, fTy) = reqSameType(pos, tenv, trexp argExp, {exp=(), ty=fTy})
                      in
                        (* check the type of each argument expression matches the corresponding formal parameter type *)
                        app checkType z;
                        {exp=doTrExp, ty=resTy}
                      end
                  end
          end

        | trexp(A.IfExp {test=testExp, then'=thenExp, else'=SOME elseExp, pos}) =
          let
            val testA = trexp testExp
            val thenA = trexp thenExp
            val elseA = trexp elseExp
            val {exp=_, ty=resTy} = thenA
          in
            checkInt(testA, pos);
            reqSameType(pos, tenv, thenA, elseA);
            {exp=doTrExp, ty=resTy}
          end

        | trexp(A.IfExp {test=testExp, then'=thenExp, else'=NONE, pos}) =
          let
            val testA = trexp testExp
            val thenA = trexp thenExp
            val {exp=_, ty=resTy} = thenA
          in
            checkInt(testA, pos);
            checkUnit(thenA, pos);
            {exp=doTrExp, ty=T.UNIT}
          end

        | trexp(A.IntExp n) = {exp=TL.constantInt n, ty=T.INT}
        | trexp(A.StringExp (s, pos)) = {exp=(TL.literalString s), ty=T.STRING}
        | trexp(A.FloatExp n) = {exp=TL.constantFloat n, ty=T.FLOAT}
        
        
        
        | trexp(A.OpExp{left, oper, right, pos}) =
          let val leftA = trexp left
              val rightA = trexp right
              val {exp=_, ty=leftTyBare} = leftA
              val {exp=_, ty=rightTyBare} = rightA
              val leftTy = actual_ty (pos, tenv, leftTyBare) 
              val rightTy = actual_ty (pos, tenv, rightTyBare) 
          in
            
            case oper 
              of (A.PlusOp | A.MinusOp | A.TimesOp | A.DivideOp) => let in
                case (leftTy, rightTy)
                  of (T.INT, T.INT) => let in
                      checkInt(leftA, pos);
		      checkInt(rightA, pos);
                      {exp=doTrExp, ty=T.INT}
                      end
                   | (T.FLOAT, T.FLOAT) => let in
                      checkFloat(leftA, pos);
                      checkFloat(rightA, pos);
                      {exp=doTrExp, ty=T.FLOAT}
                      end
                   | _ => (error pos "Types mismatch"; errorTrExpTy)  
                end
              
              | (A.LtOp | A.LeOp | A.GtOp | A.GeOp) => 
                let in
                case (leftTy, rightTy)
                  of (T.INT, T.INT) => let in
                      checkInt(leftA, pos);
		      checkInt(rightA, pos);
                      {exp=doTrExp, ty=T.INT}
                      end
                   | (T.FLOAT, T.FLOAT) => let in
                      checkFloat(leftA, pos);
                      checkFloat(rightA, pos);
                      {exp=doTrExp, ty=T.INT}
                      end
                   | _ => (error pos "Types mismatch"; errorTrExpTy) 
                end
              | (A.EqOp | A.NeqOp) => let in
                case (leftTy, rightTy)
                  of (T.INT, T.INT) => {exp=doTrExp, ty=wouldBeBooleanTy}
                   | (T.FLOAT, T.FLOAT) => {exp=doTrExp, ty=wouldBeBooleanTy}
                   | _ => (error pos "Types mismatch"; errorTrExpTy)
                end
          end

        | trexp(A.DecExp {decs, body, pos}) =
            let val {venv=venv', tenv=tenv'} = transDecs(level, venv, tenv, decs)
            in transExp(level, venv', tenv', body)
            end

        | trexp(A.SeqExp expList) = 
            let 
              val rs = map trexp (map #1 expList)
              val {exp=_, ty=lastTy} = List.last rs
            in {exp=doTrExp, ty=lastTy}
            end
        | trexp(A.Main{name,body,pos})=
            let
              val bodyA = trexp body;
            in
              checkUnit(bodyA, pos);
              {exp=doTrExp, ty=T.UNIT}
            end
       

        
            
      and trvar(A.SimpleVar(id, pos)) =
        (case S.look(venv, id)
          of SOME(E.VarEntry {access=_, ty}) => {exp=doTrExp, ty=actual_ty(pos, tenv, ty)}
           | SOME(E.FunEntry _)              => (error pos ("variable points to a function - compiler bug?: " ^ S.name id);
                                                {exp=errorTrExp, ty=T.INT})
           | NONE                            => (error pos ("undefined variable " ^ S.name id);
                                                {exp=errorTrExp, ty=T.INT})
        )
        
    in
      trexp(exp)
    end


  and transProg(exp: A.exp):unit =
    let
      val mainLevel = TL.newLevel {parent=TL.outermostLevel, name=Temp.namedLabel "main", formals=[]}
      val {exp=trExp, ty} = transExp(mainLevel, E.base_venv, E.base_tenv, exp)
    in
      TL.procEntryExit {level = mainLevel, body = trExp}
    end

end
